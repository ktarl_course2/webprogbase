function isLetter(c) { // https://stackoverflow.com/questions/9862761/how-to-check-if-character-is-a-letter-in-javascript
    return c.toLowerCase() != c.toUpperCase();
}

function nsConvert(num, baseFrom, baseTo) {
    if (num == 0 || num == 1) {
        return String(num);
    }
    function convertToDec(num_original, base_from) {
        if (typeof(num_original) != "string") {
            num_original = '' + num_original; // String(num)
        }
        let decimal_num = 0;
        for (let i = num_original.length - 1; i >= 0; i--) {
            let current_digit = num_original[i];
            if (isLetter(num_original[i])) { // so A become 10, B --> 11...
                current_digit = num_original[i].toUpperCase().charCodeAt(0) - 55; // ASCII: A <--> 65...
            }
            decimal_num += current_digit * (base_from ** (num_original.length - 1 - i));
        }
        return String(decimal_num);
    }
    function convertFromDec(num_original, base_to) {
        let num_in_new_base = "";
        while (num_original > 0) {
            const quotient = Math.trunc(num_original / base_to);
            let remainder = num_original - quotient * base_to;
            num_original = quotient;
            if (remainder > 9) {
                remainder = String.fromCharCode(remainder + 55);
            }
            num_in_new_base = String(remainder) + num_in_new_base;
        }
        return num_in_new_base;
    }
    let num_origin_to_dec = num;
    if (baseFrom != 10) {
        num_origin_to_dec = convertToDec(num, baseFrom);
    }
    let num_to_new_base = num_origin_to_dec;
    if (baseTo != 10) {
        num_to_new_base = convertFromDec(num_origin_to_dec, baseTo);
    }
    return num_to_new_base;
}

console.assert(nsConvert("23", 10, 2) === "10111", "Check 1");
console.assert(nsConvert("31", 10, 16) === "1F", "Check 2");
console.assert(nsConvert("1F", 16, 2) === "11111", "Check 3");

console.assert(nsConvert("235AB", 13, 16) === "FCBB", "Check 4");
console.assert(nsConvert("0", 9, 32) === "0", "Check 5");
console.assert(nsConvert("1", 43, 25) === "1", "Check 6");

console.assert(nsConvert("222", 4, 18) === "26", "Check 7");
console.assert(nsConvert("A11111", 11, 5) === "404022430", "Check 8");
console.assert(nsConvert("ABCDEF", 16, 3) === "210012000221220", "Check 9");
console.assert(nsConvert("ABCDEF", 16, 2) === "101010111100110111101111", "Check 10");

console.log("Finished all checks.");
