function isLetter(c) { // https://stackoverflow.com/questions/9862761/how-to-check-if-character-is-a-letter-in-javascript
    return c.toLowerCase() != c.toUpperCase();
}

function vigenereCipher(text, key) {
    text = text.toUpperCase();
    key = key.toUpperCase();
    let key_string = "";
    let i = 0;
    while (i < text.length) { // check whether text contains only letters
        if (!isLetter(text[i])) {
            text = text.split("");
            text.splice(i, 1);
        }
        else {
            i++;
        }
    }
    while (key_string.length < text.length) {
        key_string += key;
        // create key_string:
        // sometext
        // keykeykey <- key_string
    }
    key_string = key_string.split(""); // string --> array
    key_string.splice(key_string.length - 1, key_string.length - text.length); // => key_string.length = text.length
    const num_of_letters = 26;
    let encoded_message = [];
    const code_ascii_first_letter = 65;
    for (const i in text) {
        const c = (text.charCodeAt(i) + key_string[i].charCodeAt(0)) % num_of_letters + code_ascii_first_letter;
        encoded_message.push(String.fromCharCode(c));
    }
    return {
        key: key,
        message: text,
        encoded_message: encoded_message.join("")
    };
}

function vigenereDecipher(cipher, key) {
    cipher = cipher.toUpperCase();
    cipher = cipher.split(""); // string --> array
    key = key.toUpperCase();
    let key_string = "";
    let i = 0;
    while (i < cipher.length) {
        if (!isLetter(cipher[i])) {
            cipher.splice(i, 1);
        }
        else {
            i++;
        }
    }
    while (key_string.length < cipher.length) {
        key_string += key;
    }
    key_string = key_string.split(""); // string --> array
    key_string.splice(key_string.length - 1, key_string.length - cipher.length); // => key_string.length = cipher.length
    const num_of_letters = 26;
    const code_ascii_first_letter = 65;
    let decoded_message = [];
    for (const i in cipher) {
        const m = (cipher[i].charCodeAt(0) + num_of_letters - key_string[i].charCodeAt(0)) % num_of_letters + code_ascii_first_letter;
        decoded_message.push(String.fromCharCode(m));
    }
    return {
        key: key,
        message: decoded_message.join(""),
        encoded_message: cipher.join("")
    };
}
console.assert(vigenereCipher("ATTACKATDAWN", "LEMON").encoded_message === "LXFOPVEFRNHR", "Encoding 1");
console.assert(vigenereCipher("JACKANDJILLWENTUPTHEHILL", "JILL").encoded_message === "SINVJVOURTWHNVEFYBSPQQWW", "Encoding 2");
console.assert(vigenereCipher("MESSAGE", "KEY").encoded_message === "WIQCEEO", "Encoding 3");


console.assert(vigenereDecipher("LXFOPVEFRNHR", "LEMON").message === "ATTACKATDAWN", "Decoding 1");
console.assert(vigenereDecipher("SINVJVOURTWHNVEFYBSPQQWW", "JILL").message === "JACKANDJILLWENTUPTHEHILL", "Decoding 2");
console.assert(vigenereDecipher("WIQCEEO", "KEY").message === "MESSAGE", "Decoding 3");

console.log("Finished all tests.");
