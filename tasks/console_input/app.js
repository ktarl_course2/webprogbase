/*
Завдання:
    1. Створити об'єкт з полями інформації про користувача (наприклад, ім'я, прізвише, дата народження (дата, ISO8601), тощо). 
    2. Обрати спосіб вводу даних через консоль (синхронний або асинхронний). 
    3. Реалізувати можливість вивести форматовані дані з об'єкта на вимогу користувача (спеціальну команду, яку введе користувач, наприклад, print).
    4. Реалізувати можливість користувачу змінити обрані дані об'єкта за допомогою спеціальних команд, (наприклад, update/surname Kjellberg для оновлення прізвища).
    5. Перевіряти введені користувачем дані і не дозволяти задавати некоректні дані (наприклад, перевіряти формат дати ISO8601).
    6. У відповідь на кожну команду виводити статус її виконання (виконано чи ні, чому ні, яка помилка, чи існує така команда).
    7. Додати спеціальну команду користувача для виходу з програми, наприклад quit.

*/

const readline_sync = require('readline-sync');

class User {
    constructor(first_name, last_name, birthdate) {
        this.first_name_ = first_name;
        this.last_name_ = last_name;
        this.birthdate = birthdate;
    }

    set birthdate(birthdate) {
        const birthdate_var = new Date(birthdate);
        if (isNaN(birthdate_var.valueOf())) {
            throw("Invalid date input.");
            /*console.log("You should enter date in one of these formats: [December 17, 1995] or [1995-12-17]\n");
            this.birthdate_ = new Date(Date.now());*/
        }
        else {
            this.birthdate_ = birthdate_var;
        }
    }

    update(field, value_arr) {
        let message = "\nThe user was updated.\n";
        if (field === "first_name") {
            this.first_name_ = value_arr[0];
        }
        else if (field === "last_name") {
            this.last_name_ = value_arr[0];
        }
        else if (field === "birthdate") {
            try {
                user.birthdate = value_arr[0];
            }
            catch (error) {
                console.log("\n" + error);
                console.log("You should enter date in one of these formats: [December 17, 1995] or [1995-12-17]\n");
            }
            message = "\nThe user was not updated\n";
        }
        else if (field.length == 0) {
            console.log("\nThe field is empty.\n");
            message = "\nThe user was not updated.\n";
        }  
        else {
            console.log("\nUnknown field: " + field + "\n");
            message = "\nThe user was not updated.\n";
        }
        console.log(message);
    }
}

function getHelp(option = "") {
    if (option.length === 0)
    {
        console.log("\nw / write -- create a new object and write data to it\nu / update -- update object\nprint -- show object\nhelp -- get help" +
        "\nquit -- terminate program execution\n");
    }
    else if (option === "w" || option === "write") {
        console.log("\nw/first_name [first_name] -- add first name\nw/last_name [last_name] -- add last name" +
        "\nw/birthdate [birthdate] -- add birthdate\nw/all [first_name] [last_name] [birthdate] -- add all data at once\nw help -- get help\n");
    }
    else if (option === "u" || option === "update") {
        console.log("\nu/first_name [first_name] -- update first name\nw/last_name [last_name] -- update last name" + 
        "\nw/birthdate [birthdate] -- update birthdate\nu help -- get help\n");
    }
    //console.log(help_text);
}

function createAll(values) {
    let user = new User("", "", 0);
    const [first_name, last_name, birthdate] = values;
    user.first_name_ = first_name;
    user.last_name_ = last_name;
    try {
        user.birthdate = birthdate;
    }
    catch (error) {
        console.log(`\nDate: ${birthdate}\n`);
        console.log(`\n${error}`);
        console.log("You should enter date in one of these formats: [December 17, 1995] or [1995-12-17]\n");
        console.log("\nThe user was not created.\n");
        user = new User("", "", 0);
    }
    console.log("\nThe user was created.\n");
    return user;
}

function create(field, value_arr) {
    let user = new User("", "", Date.now());
    let message = "\nThe user was created.\n";
    if (field === "first_name") {
        user.first_name_ = value_arr[0];
        console.log(message);
    }
    else if (field === "last_name") {
        user.last_name_ = value_arr[0];
        console.log(message);
    }
    else if (field === "birthdate") {
        try {
            user.birthdate = value_arr[0];
        }
        catch (error) {
            console.log("\n" + error);
            console.log("You should enter date in one of these formats: [December 17, 1995] or [1995-12-17]\n");
            message = "\nThe user was not created.\n";
        }
        console.log(message);
    }
    else if (field === "all") {
        user = createAll(value_arr);
    }
    else if (field.length == 0) {
        console.log("\nThe field is empty.\n");
        message = "\nThe user was not created.\n";
        console.log(message);
    }  
    else {
        console.log(`\nUnknown field: ${field}\n`);
        message = "\nThe user was not created.\n";
        console.log(message);
    }
    return user;
}

function print(user) {
    console.log(`\nFirst name: ${user.first_name_}\nLast name: ${user.last_name_}\nBirthdate: ${user.birthdate_.toDateString()} \n`);
}

getHelp();
let user = new User("", "", 0);
let command = readline_sync.question("").trim();
//console.log("Your command: " + command);
while (command != "quit") { 
    // w/first_name John --> 
    // command_full = w/first_name, data = John, 
    // command_type = w, command_field = first_name
    const [command_full, ...data] = command.split(' ');
    const [command_type = "", command_field = ""] = command_full.split('/');
    if (command === "help") {
        getHelp();
    }
    else if (command == "print") {
        print(user);
    }
    else if (data[0] === "help") { // w help
        getHelp(command_full);
    }
    else if (command_type == "w" || command_type == "write") {
        user = create(command_field, data);
    }
    else if (command_type == "u" || command_type == "update") {
        user.update(command_field, data);
    }
    else {
        console.log(`Unknown command: ${command}`);
    }
    command = readline_sync.question("").trim();
}