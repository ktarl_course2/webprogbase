const JsonStorage = require('./jsonStorage.js');
/*
Використання розробленого модуля:

    1 Імпортувати модуль і використати його для роботи з файлом data/users.json
    2 Вивести всі записи зі сховища
    3 Вивести значення наступного ідентифікатора
    4 Інкрементувати наступний ідентифікатор
    5 Додати до записів сховища новий запис і зберегти записи у сховище

*/

const file_path = './data/users.json';
const id_file_path = './data/users_id.txt';
const json_storage = new JsonStorage(file_path, id_file_path);

console.log(json_storage.readItems());
console.log("\n" + json_storage.nextId);

const next_id = json_storage.nextId;
json_storage.incrementNextId();

const user = {
    "id": next_id,
    "login": "testUser 2",
    "fullname": "Test User 2"
};

let items = json_storage.readItems()
items.push(user);
json_storage.writeItems(items);

console.log(json_storage.readItems());
