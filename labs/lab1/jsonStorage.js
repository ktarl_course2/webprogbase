const fs = require('fs');

class JsonStorage {
    // file_path - path to JSON file
    constructor(file_path, id_file_path) {
        this.file_path = file_path;
        this.id_file_path = id_file_path;
    }

    get nextId() {
        const id_string = fs.readFileSync(this.id_file_path, 'utf8');
        const json_object_id = JSON.parse(id_string); 
        const next_id = parseInt(json_object_id.next_id, 10);
        return next_id;
    }

    incrementNextId() {
        const id = { "next_id": this.nextId + 1 };
        const json_object = JSON.stringify(id, null, 4);
        fs.writeFileSync(this.id_file_path, json_object);
    }

    readItems() {
        const json_text = fs.readFileSync(this.file_path, 'utf8');
        const json_array = JSON.parse(json_text);
        return json_array;
    }

    writeItems(items) {
        const json_text = JSON.stringify(items, null, 4);
        fs.writeFileSync(this.file_path, json_text);
    }
};

module.exports = JsonStorage;