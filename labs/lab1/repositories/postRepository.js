const Post = require('../models/post');
const JsonStorage = require('../jsonStorage');

function jsonObjectToPostModel(json_object) {
    const plain_post_model = new Post();
    const post_model = Object.assign(plain_post_model, json_object);
    return post_model;
}

function isValidId(id) {
    return typeof id !== "undefined" && id !== null && !isNaN(id);
}

class PostRepository {
    constructor(file_path, id_file_path) {
        this.storage = new JsonStorage(file_path, id_file_path);
    }

    getPosts() { 
        const json_array = this.storage.readItems();
        const posts_array = json_array.map(json_object => jsonObjectToPostModel(json_object));
        return posts_array;
    }
 
    getPostById(postId) {
        const posts_array = this.getPosts();
        const post = posts_array.find(post => post.id_ === postId);
        if (typeof post === "undefined") { 
            console.error(`Post with id ${postId} does not exist\n`);
            return null;
        }
        return post;
    }
 
    addPost(postModel) { // return id
        const id = this.storage.nextId;
        if (!isValidId(id)) {
            console.error("\nInvalid type of next id: undefined\n");
            return;
        }
        const posts_array = this.getPosts();
        postModel.id = id;
        posts_array.push(postModel);
        this.storage.writeItems(posts_array);
        this.storage.incrementNextId();
        return id;
    }
 
    updatePost(postModel) { // return boolean
        let posts_array = this.getPosts();
        const index = posts_array.findIndex(post => post.id_ === postModel.id);
        if (index< 0) {
            console.error(`Post with id ${postModel.id} does not exist\n`);
            return false;
        }
        posts_array[index] = postModel;
        this.storage.writeItems(posts_array);
        return true;
    }
 
    deletePost(postId) { // return boolean
        let posts_array = this.getPosts();
        const index = posts_array.findIndex(post => post.id_ === postId);
        if (typeof index < 0) {
            console.error(`Post with id ${postId} does not exist\n`);
            return false;
        }
        posts_array.splice(index, 1);
        this.storage.writeItems(posts_array);
        return true;
    }
};

module.exports = PostRepository;