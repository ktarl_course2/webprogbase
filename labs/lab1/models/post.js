class Post {
    constructor(id = 0, heading = "", text = "", posted_at = 0, likes = 0, views = 0) {
        this.id = id;  // number
        this.heading = heading;  // string
        this.text = text;  // string
        this.posted_at = posted_at; // date string (ISO 8601)
        this.likes = likes; // number
        this.views = views; // number
    }

    set id(id) {
        if (typeof id === "undefined") {
            console.error("\nInvalid user id\n");
            this.id_ = 0;
        } else { this.id_ = parseInt(id, 10); }
    }

    get id() {
        return this.id_;
    }

    set posted_at(date) {
        const date_var = new Date(date);
        if (isNaN(date_var.valueOf())) {
            console.error("\nInvalid date input\n");
            this.posted_at_ = new Date(0).toString; //
        }
        else {
            this.posted_at_ = date_var.toISOString();
        }
    }

    set likes(likes) {
        const likes_num = parseInt(likes);
        if (likes_num < 0) { console.error("\nInvalid number of likes\n"); }
        else { this.likes_ = likes_num; }
    }

    set views(views) {
        const views_num = parseInt(views, 10);
        if (views_num < 0) { console.error("\nInvalid number of views\n"); }
        else { this.views_ = views_num; }
    }
};

module.exports = Post;