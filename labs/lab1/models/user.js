class User {
    constructor(id = 0, login = "", fullname = "", role = 0, registered_at = 0, ava_url = "", is_enabled = false) {
        this.id = id;  // number
        this.login = login;  // string
        this.fullname = fullname;  // string
        this.role = role; // number: 0 or 1
        this.registered_at = registered_at; // date string (ISO 8601)
        this.ava_url = ava_url; // string
        this.is_enabled = is_enabled; // boolean
    }

    set id(id) {
        if (typeof id === "undefined") {
            console.error("\nInvalid user id");
            this.id_ = 0;
        } else { this.id_ = parseInt(id, 10); }
    }

    get id() {
        return this.id_;
    }

    set role(role) {
        const role_num = parseInt(role);
        if (role_num !== 0 && role_num !== 1) { console.error(`\nInvalid user role: ${role}`); }
        else { this.role_ = role_num; }
    }

    set registered_at(date) {
        const date_var = new Date(date);
        if (isNaN(date_var.valueOf())) {
            console.error("\nInvalid date input");
            this.registered_at_ = new Date(0).toString; //
        }
        else {
            this.registered_at_ = date_var.toISOString();
        }
    }

    set is_enabled(is_enabled) {
        if (is_enabled !== "true" && is_enabled !== "false" && typeof is_enabled !== "boolean") { 
            console.error(`\nInvalid input: property 'is_enabled' should be true/false`); 
        } else { this.is_enabled_ = JSON.parse(is_enabled); } 
    }
};

module.exports = User;
