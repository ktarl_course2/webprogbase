// cd Study/"II course"/Web/webprogbase/labs/lab1
const User = require('./models/user');
const Post = require('./models/post');
const UserRepository = require('./repositories/userRepository');
const PostRepository = require('./repositories/postRepository');
const readline_sync = require('readline-sync');

function getHelp() {
    console.log(`
                 get/{entities}         -- get all entities
                 get/{entities}/{id}    -- get entity by id
                 delete/{entities}/{id} -- delete entity by id
                 update/{entities}/{id} -- enter new data and update the entity
                 post/{entities}        -- add new entity
                
                 help                   -- get help
                 quit                   -- terminate program execution\n`);
}

function getHelpUser() {
    console.log(`
                 login/{login}                      -- set login
                 fullname/{fullname}                -- set fullname
                 role/{role:0/1}                    -- set role (0 -- ordinary user, 1 -- admin)
                 registered_at/{registered_at}      -- set date (ISO 8601)
                 ava_url/{ava_url}                  -- set URL of picture
                 is_enabled/{is_enabled:true/false} -- set if is (not) active
                 save                               -- save and quit edit mode
                 quit                               -- save and quit edit mode\n`);
}

function getHelpPost() {
    console.log(`
                 heading/{heading}     -- set heading
                 text/{text}           -- set text
                 posted_at/{posted_at} -- set date (ISO 8601)
                 likes/{likes}         -- set number of likes
                 views/{views}         -- set number of views
                 save                  -- save and quit edit mode
                 quit                  -- quit edit mode\n`);
}

function get(entities, id) {
    if (typeof id === "undefined" || isNaN(id)) { // get all entities
        if (entities === "users") { // get all users
            const users_array = user_repository.getUsers();
            console.table(users_array);
        }
        else if (entities === "posts"){ // get all posts
            const posts_array = post_repository.getPosts();
            console.table(posts_array);
        }
    }
    else { // get entity by id
        if (entities === "users") { // get user by id
            const user = user_repository.getUserById(id);
            if (user) { console.table(user); }
        }
        else if (entities === "posts"){ // get post by id
            const post = post_repository.getPostById(id);
            if (post) { console.table(post); }
        }
    }
}

function delete_(entities, id) { // delete entity by id
    if (entities === "users") { // get user by id
        const user = user_repository.getUserById(id);
        if (typeof user !== "undefined" && user) { // user != null
            console.log("\nUser to delete: ");
            console.table(user); 
            const answer = readline_sync.question("\nAre you sure you want to delete this user? [y/n] ").trim().toLowerCase();
            if (answer === "y" && !user_repository.deleteUser(id)) { console.error("\nThe user was not deleted\n"); }
            else if (answer === "y") { console.log("\nThe user was deleted\n"); }
        }
    }
    else if (entities === "posts") { // get post by id
        const post = post_repository.getPostById(id);
        if (typeof post !== "undefined" && post) { // post != null
            console.log("\nPost to delete: ");
            console.table(post); 
            const answer = readline_sync.question("\nAre you sure you want to delete this user? [y/n] ").trim().toLowerCase();
            if (answer === "y" && !post_repository.deletePost(id)) { console.error("\nThe post was not deleted\n"); }
            else if (answer === "y") { console.log("\nThe post was deleted\n"); }
        } 
    }
}

function getUserInfoConsole() {
    let user = new User();
    getHelpUser();
    let command = readline_sync.question("").trim();
    let [field, value] = command.split('/');
    
    while (command !== "quit" && command !== "save") {
        if (user.hasOwnProperty(field) || user.hasOwnProperty(field+"_")) { user[field] = value; }
        else { console.error(`\nUsers do not have this property: ${field}`); }
        command = readline_sync.question("").trim();
        [field, value] = command.split('/');
    }
    if (command === "save") {
        return user;
    } else { return null; }
}

function getPostInfoConsole() {
    let post = new Post();
    getHelpPost();
    let command = readline_sync.question("").trim();
    let [field, value] = command.split('/');
    
    while (command !== "quit" && command !== "save") {
        if (post.hasOwnProperty(field) || post.hasOwnProperty(field+"_")) { post[field] = value; }
        else { console.error(`\nPosts do not have this property: ${field}`); }
        command = readline_sync.question("").trim();
        [field, value] = command.split('/');
    }
    if (command === "save") {
        return post;
    } else { return null; }
}

function update(entities, id) {
    if (entities === "users") { // update user by id
        const user = user_repository.getUserById(id);
        if (typeof user !== "undefined" && user) {// user != null
            console.table(user);
            const updated_user = getUserInfoConsole();
            if (updated_user) { // user !== null
                updated_user.id = id; // !
                if (!user_repository.updateUser(updated_user)) { console.error("\nThe user was not updated\n"); }
                else { console.log("\nThe user was updated\n"); }
            }
            else { console.log("\nThe user was not updated\n"); }
        }
    }
    else if (entities === "posts") { // update post by id
        const post = post_repository.getPostById(id);
        if (typeof post !== "undefined" && post) { // post != null
            console.table(post);
            const updated_post = getPostInfoConsole();
            if (updated_post) { // post !== null
                updated_post.id = id; // !
                if (!post_repository.updatePost(updated_post)) { console.error("\nThe post was not updated\n"); }
                else { console.log("\nThe post was updated\n"); }
            }
            else { console.log("\nThe post was not updated\n"); }
        }
    }
}

function post(entities) { // create new entity
    if (entities === "users") { // create user
            const user = getUserInfoConsole();
            if (user) { // user !== null
                const id = user_repository.addUser(user);
                console.log(`\nNew user id: ${id}`);
            }
            else { console.log("\nThe user was not added\n"); }
    }
    else if (entities === "posts") { // create post
        const post = getPostInfoConsole();
        if (post) { // post !== null
            const id = post_repository.addPost(post);
            console.log(`\nNew post id: ${id}`);
        }
        else { console.log("\nThe post was not added\n"); }
    }
}

const users_file_path = "./data/users.json";
const users_id_file_path = "./data/users_id.json";
const posts_file_path = "./data/posts.json";
const posts_id_file_path = "./data/posts_id.json";

const user_repository = new UserRepository(users_file_path, users_id_file_path);
const post_repository = new PostRepository(posts_file_path, posts_id_file_path);

function isCommandValid(command) {
    const [command_type, entities, id_string, ...remainder] = command.split('/');
    if (typeof command_type === "undefined" || typeof command === "undefined" || command.length === 0) {
        console.error("\nCommand was not entered\n");
        return false;
    }
    else if (typeof command_types.find(command => command === command_type) === "undefined\n") {
        console.error(`\nUnknown command: ${command_type}\n`); 
        return false;
    }
    else if (typeof entities === "undefined\n") {
        console.error(`\n${command_type} what?\n`);
        return false;
    }
    else if (entities !== "posts" && entities !== "users") { 
        console.error(`\nUnknown type of entities: ${entities}\n`); 
        return false;
    }
    else if ((command_type === "delete" || command_type === "update") && (typeof id_string === "undefined\n")) { 
        console.error(`\n${command_type} which?\n`); 
        return false;
    }
    else if (typeof remainder !== "undefined" && remainder.length > 0) {
        console.error(`Command too long, unexpected parameters: ${remainder}\n`);
        return false;
    }
    return true;
}

const command_types = ["get", "post", "update", "delete"];
getHelp();
let command = readline_sync.question("").trim();
while (command !== "quit") {
    const [command_type, entities, id_string] = command.split('/');
    const id = parseInt(id_string, 10);
    if (command_type === "help") { getHelp(); }
    else if (isCommandValid(command)){
        if (command_type === "get") { 
            get(entities, id); 
        }
        else if (command_type === "delete") { 
            delete_(entities, id); 
        }
        else if (command_type === "update") { 
            update(entities, id); 
        }
        else if (command_type === "post") { 
            post(entities); 
        }
    }
    command = readline_sync.question("").trim();
}
