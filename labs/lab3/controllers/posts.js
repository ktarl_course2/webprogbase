const Post = require('../models/post');
const PostRepository = require('../repositories/post_repository');
const validator = require('./validator');
const MediaRepository = require('../repositories/media_repository')
const page_handler = require('./page_handler');

const posts_file_path = "./data/posts.json";
const posts_id_file_path = "./data/posts_id.json";
const post_repository = new PostRepository(posts_file_path, posts_id_file_path);

const mediaDirPath = "./data/media";
const mediaIdFilePath = "./data/media/next_id.txt";
const mediaRepository = new MediaRepository(mediaDirPath, mediaIdFilePath);

let currentPage = 1;
let numberOfPages = 0;

function isSearchOptionSelected(headingToSearch) {
    return typeof headingToSearch != "undefined" && headingToSearch != null;
}

function getPostsWithHeadingMatched(posts, headingToSearch) {
    let filteredPosts = posts.filter(post =>
        post.heading_.toLowerCase().includes(headingToSearch.toLowerCase())
    );
    return filteredPosts;
}

function getHrefOnPrevPageButton(currentPage) {
    if (currentPage === 1) {
        return `/posts?page=1`;
    }
    return `/posts?page=${currentPage-1}`;
}

function getHrefOnNextPageButton(currentPage, numberOfPages) {
    if (numberOfPages <= currentPage) {
        return `/posts?page=${numberOfPages}`;
    }
    return `/posts?page=${currentPage+1}`;
}

module.exports = {
    getPosts(req, res) {
        let posts = post_repository.getPosts();
        numberOfPages = page_handler.calculateNumberOfPages(posts.length);
        if (typeof req.query.page !== "undefined") {
            currentPage = page_handler.checkAndGetPaginationParams(req.query.page).page;
        }
        const headingToSearch = req.query.search;
        if (isSearchOptionSelected(headingToSearch)) {
            posts = getPostsWithHeadingMatched(posts, headingToSearch);
        }
        const fromToParams = page_handler.getFromToParams(currentPage, posts.length);
        numberOfPages = page_handler.calculateNumberOfPages(posts.length);
        posts = posts.slice(fromToParams.from, fromToParams.to);
        const hrefOnPrevPageButton = getHrefOnPrevPageButton(currentPage);
        const hrefOnNextPageButton = getHrefOnNextPageButton(currentPage, numberOfPages);
        res.status(200).render("posts", {
            posts,
            headingToSearch,
            numberOfPages,
            currentPage,
            hrefOnPrevPageButton,
            hrefOnNextPageButton,
            minNumberOfPage: 1,
            maxNumberOfPage: numberOfPages
        });
    },

    getPostById(req, res) {
        const id = req.params.id;
        let parsedId = 0;
        try {
            parsedId = Number.parseInt(id);
        }
        catch (error) {
            console.error(error);
            res.status(400).render("post", {
                heading_: "Not found"
            })
        }
        const post = post_repository.getPostById(parsedId);
        if (post === null) {
            res.status(404).render("post", {
                heading_: "Not found"
            });
        }
        else {
            res.status(200).render("post", post);
        }
    },

    showAddPostForm(req, res) {
        res.status(200).render("add_post");
    },

    addPost(req, res) {
        const post = req.body;
        if (post === null) {
            res.status(400).render("post", {
                heading_: "Not found"
            });
        }
        else {
            post.postedAt = Date.now();
            if (typeof req.files !== 'undefined' && typeof req.files['image'] !== 'undefined') {
                const fileData = req.files['image'].data;
                const fileName = req.files['image'].name;
                if (!validator.isFileNameFull(fileName)) {
                    res.status(400).redirect(`/posts/0`);
                } else {
                    const mediaId = mediaRepository.addMedia(fileData, fileName);
                    post.imageUrl_ = `/media/${mediaId}`;
                }
            }
            const id = post_repository.addPost(post);
            res.redirect(`/posts/${id}`);
        }
    },

    deletePost(req, res) {
        const idToFind = req.params.id;
        let posts = post_repository.getPosts();
        const indexPostToDelete = posts.findIndex(post => post.id_ == idToFind);
        if (indexPostToDelete < 0) {
            const error_message = `Post with id ${id_to_find} does not exist`;
            console.error(`${error_message}\n`);
            res.status(404).end();
        }
        else {
            console.log(indexPostToDelete);
            posts.splice(indexPostToDelete, 1);
            post_repository.storage.writeItems(posts);
            res.status(200).end();
        }
    }
};
