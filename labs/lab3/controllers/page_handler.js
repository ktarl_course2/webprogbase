module.exports = {
    checkAndGetPaginationParams(page, perPage, defaultPage = 1,
                                defaultPerPage = 10, defaultMaxPerPage = 100) {
        if (typeof page == 'undefined') {
            return {
                page: defaultPage,
                perPage: defaultPerPage
            };
        }
        try {
            page = Number.parseInt(page);
        }
        catch (e) { return null; }
        if (typeof perPage == 'undefined') {
            return {
                page: page,
                perPage: defaultPerPage
            };
        }
        try {
            perPage = Number.parseInt(perPage);
        }
        catch (e) { return null; }
        return {
            page: page,
            perPage: perPage <= defaultMaxPerPage ? perPage : defaultMaxPerPage
        };
    },

    getFromToParams(page, max_number_of_entities, per_page = 10) {
        return {
            from: (page - 1) * per_page,
            to: (page * per_page + 1 <= max_number_of_entities) ? (page * per_page + 1) : max_number_of_entities
        };
    },

    calculateNumberOfPages(numberOfEntities, perPage = 10) {
        if (numberOfEntities === 0) {
            return 1;
        }
        const float = numberOfEntities / perPage;
        const floor = Math.floor(numberOfEntities / perPage);
        return floor === float ? floor : floor + 1;
    }
}