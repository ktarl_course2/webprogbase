const Post = require('../models/post');
const User = require('../models/user');

module.exports = {
    checkAndGetPost(json_object) {
        const post = new Post();
        let required_keys = [];
        for (const key of Object.keys(post)) {
            if (key != "id" && key != "id_") {
                required_keys.push(key);
            }
        }
        for (const key of Object.keys(json_object)) {
            if (post.hasOwnProperty(key) || post.hasOwnProperty(key + "_")) {
                try {
                    post[key] = json_object[key];
                    required_keys = required_keys.filter(required_key => required_key != key && required_key != key+"_");
                }
                catch (e) {
                    console.error(e);
                    return null;
                }
            }
            else {
                console.error(`No such property in post: ${key}`);
                return null;
            }
        }
        if (required_keys.length > 0) {
            console.error(`Required data not provided: ${required_keys}`);
            return null;
        }
        return post;
    },

    isFileNameFull(file_name) {
        const [name, format] = file_name.split('.');
        if (typeof name === 'undefined' || typeof format === 'undefined') {
            return false;
        }
        if (name.length === 0 || format.length === 0) {
            return false;
        }
        return true;
    }
};
