const User = require('../models/user');
const UserRepository = require('../repositories/user_repository');

const users_file_path = "./data/users.json";
const users_id_file_path = "./data/users_id.json";
const userRepository = new UserRepository(users_file_path, users_id_file_path);


module.exports = {
    getUsers(req, res) {
        let users = userRepository.getUsers();
        res.status(200).render("users", {
            users,
        });
    },

    getUserById(req, res) {
        const id = req.params.id;
        let parsedId = 0;
        try {
            parsedId = Number.parseInt(id);
        }
        catch (error) {
            res.status(400).render("user.mst", {
                fullname: "Not found",
            });
        }
        const user = userRepository.getUserById(parsedId);
        if (user === null || typeof user === undefined) {
            res.status(404).render("user.mst", {
                fullname: "Not found",
            });
        }
        else {
            console.log("get user");
            res.status(200).render("user.mst", user);
        }
    }
};
