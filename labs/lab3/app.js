const express = require('express');
const body_parser = require('body-parser');
const busboy = require('busboy-body-parser');
const apiRouter = require('./routes/api');
const consolidate = require('consolidate');
const mustache = require('mustache-express');
const path = require('path');
const morgan = require('morgan');

const app = express();
const port = 3000;

// will open public/ directory files for http requests
app.use(express.static('public'));

app.use(body_parser.urlencoded({ extended: true }));
app.use(body_parser.json());
const busboy_options = {
    limit: '5mb',
    multi: false,
};
app.use(busboy(busboy_options));

const viewsDir = path.join(__dirname, 'views');
app.engine("mst", mustache(path.join(viewsDir, "partials")));
app.set('views', viewsDir);
app.set('view engine', 'mst');

app.use(morgan('dev'));

app.use('/', apiRouter);

app.listen(port, () => {
    console.log(`Server is listening at port ${port}`);
});
