class User {
    constructor(id = 0, login = "", fullname = "", role = 0, registeredAt = 0,
                avaUrl = "", isEnabled = false, bio = "") {
        this.id = id;  // number
        this.login_ = login;  // string
        this.fullname_ = fullname;  // string
        this.role = role; // number: 0 or 1
        this.registeredAt = registeredAt; // date string (ISO 8601)
        this.avaUrl_ = avaUrl; // string
        this.isEnabled = isEnabled; // boolean
        this.bio_ = bio;
    }

    set id(id) {
        if (typeof id === "undefined") {
            console.error("\nInvalid user id");
            this.id_ = 0;
            throw Error('Invalid user id');
        } else { this.id_ = parseInt(id, 10); }
    }

    set role(role) {
        const role_num = parseInt(role);
        if (role_num !== 0 && role_num !== 1) {
            console.error(`\nInvalid user role: ${role}`);
            throw Error(`Invalid user role: ${role}`);
        }
        else { this.role_ = role_num; }
    }

    set registeredAt(date) {
        const dateVar = new Date(date);
        if (isNaN(dateVar.valueOf())) {
            console.error("\nInvalid date input");
            this.registeredAt_ = new Date(0).toString; //
            throw Error(`Invalid date input: ${date}`);
        }
        else {
            this.registeredAt_ = dateVar.toISOString();
        }
    }

    set isEnabled(isEnabled) {
        if (isEnabled !== "true" && isEnabled !== "false" && typeof isEnabled !== "boolean") { 
            console.error(`\nInvalid input: property 'is_enabled' should be true/false`);
            throw Error(`Invalid input: '${isEnabled}': property 'is_enabled' should be true/false`);
        } else { this.isEnabled_ = JSON.parse(isEnabled); }
    }
};

module.exports = User;
