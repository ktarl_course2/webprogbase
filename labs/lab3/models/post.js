/**
 * @typedef Post
 * @property {integer} id
 * @property {string} heading.required
 * @property {string} text.required
 * @property {string} postedAt.required - string in date format (e.g. DD-MM-YYYY)
 * @property {integer} likes.required
 * @property {integer} views.required
 */


class Post {
    constructor(id = 0, heading = "", text = "", postedAt = 0, likes = 0, views = 0,
                imgUrl = "") {
        this.id = id;  // number
        this.heading_ = heading;  // string
        this.text_ = text;  // string
        this.postedAt = postedAt; // date string (ISO 8601)
        this.likes = likes; // number
        this.views = views; // number
        this.imgUrl_ = imgUrl;
    }

    set id(id) {
        if (typeof id === "undefined") {
            console.error("\nInvalid user id\n");
            this.id_ = 0;
            throw Error('Invalid user id');
        } else { this.id_ = parseInt(id, 10); }
    }

    set postedAt(date) {
        const dateObject = new Date(date);
        if (isNaN(dateObject.valueOf())) {
            this.postedAt_ = new Date(0).toString; //
            throw Error("Invalid date input");
        }
        else {
            this.postedAt_ = dateObject.toISOString();
        }
    }

    set likes(likes) {
        const likes_num = parseInt(likes);
        if (likes_num < 0) {
            throw Error('Invalid number of likes');
        }
        else { this.likes_ = likes_num; }
    }

    set views(views) {
        const views_num = parseInt(views, 10);
        if (views_num < 0) {
            throw Error('Invalid number of views');
        }
        else { this.views_ = views_num; }
    }
};

module.exports = Post;