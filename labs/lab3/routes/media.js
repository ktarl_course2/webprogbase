const express = require("express");
const media_controller = require("../controllers/media");

const router = express.Router();

router
    .get("/:id", media_controller.getMediaById)
    .post("/", media_controller.addMedia);

module.exports = router;