const express = require("express");
const userController = require("../controllers/users");

const router = express.Router();

router
    .get("/", userController.getUsers)
    .get("/:id", userController.getUserById);

module.exports = router;