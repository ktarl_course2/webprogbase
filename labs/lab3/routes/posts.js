const express = require("express");
const post_controller = require("../controllers/posts");

const router = express.Router();

router
    .get("/new", post_controller.showAddPostForm)
    .get("/:id", post_controller.getPostById)
    .get("/", post_controller.getPosts)
    .post("/new", post_controller.addPost)
    .post("/:id", post_controller.deletePost);

module.exports = router;

