const express = require('express');
const posts_router = require('./posts');
const users_router = require('./users');
const about_router = require('./about');
const media_router = require('./media');

const router = express.Router();

router
    .use("/users", users_router)
    .use("/posts", posts_router)
    .use("/about", about_router)
    .use("/media", media_router)
    .get("/", function(req, res) {
        res.render('index'); }
    );


module.exports = router;