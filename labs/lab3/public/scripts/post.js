window.onload = async () => {
    let deletePostButton = document.getElementById("deletePostButton");
    console.log(deletePostButton);
    deletePostButton.addEventListener("click", async () => {
        let response = await fetch(window.location.href, {
            method: "POST"
        }).then(() => {
                alert("Post was deleted");
                window.location.assign("/posts");
            }
        )
    });
}
