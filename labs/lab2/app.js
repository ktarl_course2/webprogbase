// cd Study/"II course"/Web/webprogbase/labs/lab2
const expressSwaggerGenerator = require('express-swagger-generator');
const express = require('express');
const body_parser = require('body-parser');
const busboy = require('busboy-body-parser');
const api_router = require('./routes/api');

const app = express();
const port = 3000;

app.use(body_parser.urlencoded({ extended: false }));
app.use(body_parser.json());

const busboy_options = {
    limit: '5mb',
    multi: false,
};
app.use(busboy(busboy_options));

const expressSwagger = expressSwaggerGenerator(app);
const express_swagger_options = {
    swaggerDefinition: {
        info: {
            description: 'Allows to get data about users and perform CRUD operations on posts',
            title: 'Lab2: JSON HTTP API web server',
            version: '1.0.0',
        },
        host: `localhost:${port}`,
        produces: [ "application/json" ],
    },
    basedir: __dirname,
    files: ['./routes/**/*.js', './models/**/*.js'],
};
expressSwagger(express_swagger_options);

app.use('/api', api_router);
app.listen(port, () => {
    console.log(`Server is listening at port ${port}`);
});
