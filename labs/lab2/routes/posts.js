const express = require("express");
const post_controller = require("../controllers/posts");

const router = express.Router();

router
    /**
     * Get a post by ID
     * @route GET /api/posts/{id}
     * @group Posts
     * @param {integer} id.path.required - unique ID of the post
     * @returns {Post.model} 200 - Post object
     * @returns {Error} 404 - Post not found
     */
    .get("/:id", post_controller.getPostById)
    /**
     * Get all posts on a page
     * @route GET /api/posts
     * @group Posts
     * @param {integer} page.query - number of a page (starts from 1), is set to default (1) if not provided
     * @param {integer} per_page.query - number of items per page (1--100), is set to default (10) if not provided
     * @returns {Array<Post>} 200 - Array of post objects (empty if there are no posts)
     * @returns {Error} 400 - Invalid page or per_page parameter
     */
    .get("/", post_controller.getPosts)
    /**
     * Create a new post
     * @route POST /api/posts
     * @group Posts
     * @param {Post.model} post.body.required - New post object (without ID)
     * @returns {Post.model} 201 - Post was added. Returns post object with new ID
     * @returns {Error} 400 - Invalid keys or values in a post object from request
     */
    .post("/", post_controller.addPost)
    /**
     * Update an existing post by ID
     * @route PUT /api/posts/{id}
     * @group Posts
     * @param {Post.model} post.body.required - Post object with updated values (without ID)
     * @returns {Post.model} 200 - Post was updated. Returns post object
     * @returns {Error} 400 - Invalid keys or values in a post object from request
     * @returns {Error} 404 - Post not found
     */
    .put("/:id", post_controller.updatePost)
    /**
     * Delete post by ID
     * @route DELETE /api/posts/{id}
     * @group Posts
     * @param {integer} id.path.required - ID of a post to delete
     * @returns {Post.model} 200 - Post was deleted. Returns post object, which was deleted
     * @returns {Error} 404 - Post not found
     */
    .delete("/:id", post_controller.deletePost);

module.exports = router;

