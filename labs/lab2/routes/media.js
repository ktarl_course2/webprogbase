const express = require("express");
const media_controller = require("../controllers/media");

const router = express.Router();

router
    /**
     * Get an image by ID
     * @route GET /api/media/{id}
     * @group Media
     * @param {integer} id.path.required - unique ID of the file (was returned when the file was added to the storage)
     * @returns {file} 200 - image found
     * @returns {Error} 404 - image not found
     */
    .get("/:id", media_controller.getMediaById)
    /**
     * Get all users on a page
     * @route POST /api/media
     * @group Media
     * @consumes multipart/form-data
     * @param {file} image.formData.required - uploaded image
     * @returns {integer} 200 - image ID
     * @returns {Error} 400 - no file provided or invalid file name (without extension)
     */
    .post("/", media_controller.addMedia);

module.exports = router;