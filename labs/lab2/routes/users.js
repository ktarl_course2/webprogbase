const express = require("express");
const user_controller = require("../controllers/users");

const router = express.Router();

router
    /**
     * Get all users on a page
     * @route GET /api/users
     * @group Users
     * @param {integer} page.query - number of a page (starts from 1), is set to default (1) if not provided
     * @param {integer} per_page.query - number of items per page (1--100), is set to default (10) if not provided
     * @returns {Array<User>} 200 - Array of user objects (empty if there are no users)
     * @returns {Error} 400 - Invalid page or per_page parameter
     */
    .get("/", user_controller.getUsers)
    /**
     * Get a user by ID
     * @route GET /api/users/{id}
     * @group Users
     * @param {integer} id.path.required - unique ID of the user
     * @returns {User.model} 200 - User object
     * @returns {Error} 404 - User not found
     */
    .get("/:id", user_controller.getUserById);

module.exports = router;