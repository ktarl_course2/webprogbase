const express = require('express');
const posts_router = require('./posts');
const users_router = require('./users');
const media_router = require('./media');

const router = express.Router();

router
    .use("/users", users_router)
    .use("/posts", posts_router)
    .use("/media", media_router);


module.exports = router;