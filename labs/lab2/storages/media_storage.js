const fs = require('fs');
const find = require('find');
const glob = require('glob');
const path = require('path')
// find.use({fs: fs});

class MediaStorage {
    // directory_path - path to directory with media (e.g. images)
    constructor(directory_path, id_file_path) {
        this.directory_path = directory_path;
        this.id_file_path = id_file_path;
    }

    get nextId() {
        const id_string = fs.readFileSync(this.id_file_path, 'utf8');
        const next_id = Number.parseInt(id_string, 10);
        return next_id;
    }

    incrementNextId() {
        const incremented_id = this.nextId + 1;
        fs.writeFileSync(this.id_file_path, incremented_id);
    }

    readMediaFile(id) {
        const files = fs.readdirSync(this.directory_path);
        let target_file = files.filter(function(file) {
            const basename = path.basename(file);
            const [name, extension] = basename.split('.');
            return (typeof name != 'undefined' && name === id.toString());
        });
        if (typeof target_file != 'undefined' && target_file.length > 0) {
            return {
                name: this.directory_path + target_file.toString(),
                data: fs.readFileSync(this.directory_path + target_file.toString())
            };
        }
        return null;
    }

    writeMediaFile(file_data, file_name) {
        const id = this.nextId;
        const [name, format] = file_name.split('.');
        const file_name_with_id = `${id}.${format}`
        fs.writeFileSync(this.directory_path + file_name_with_id, file_data);
        this.incrementNextId();
        return id;
    }
}

module.exports = MediaStorage;