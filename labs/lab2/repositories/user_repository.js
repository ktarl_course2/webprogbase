const User = require('../models/user');
const JsonStorage = require('../storages/json_storage');

function jsonObjectToUserModel(json_object) {
    const plain_user_model = new User();
    const user_model = Object.assign(plain_user_model, json_object);
    return user_model;
}

function isValidId(id) {
    return typeof id !== "undefined" && id !== null && !isNaN(id);
}
 
class UserRepository {
 
    constructor(file_path, id_file_path) {
        this.storage_ = new JsonStorage(file_path, id_file_path);
    }

    get storage() {
        return this.storage_;
    }
 
    getUsers() { 
        const json_array = this.storage_.readItems();
        const users_array = json_array.map(json_object => jsonObjectToUserModel(json_object));
        return users_array;
    }
 
    getUserById(userId) {
        const users_array = this.getUsers();
        const user = users_array.find(user => user.id_ === userId);
        if (typeof user === "undefined") {
            console.error(`User with id ${userId} does not exist\n`);
            return null;
        }
        return user;
    }
 
    addUser(userModel) { // return id
        const id = this.storage_.nextId;
        if (!isValidId(id)) {
            console.error("\nInvalid type of next id: undefined\n");
            return;
        }
        const users_array = this.getUsers();
        userModel.id = id;
        users_array.push(userModel);
        this.storage_.writeItems(users_array);
        this.storage_.incrementNextId();
        return id;
    }
 
    updateUser(userModel) { // return boolean
        let users_array = this.getUsers();
        const index = users_array.findIndex(user => user.id_ === userModel.id);
        if (index < 0) {
            console.error(`User with id ${userModel.id} does not exist\n`);
            return false;
        }
        users_array[index] = userModel;
        this.storage_.writeItems(users_array);
        return true;
    }
 
    deleteUser(userId) { // return boolean
        let users_array = this.getUsers();
        const index = users_array.findIndex(user => user.id_ === userId);
        if (index < 0) {
            console.error(`User with id ${userId} does not exist\n`);
            return false;
        }
        users_array.splice(index, 1);
        this.storage_.writeItems(users_array);
        return true;
    }
};
 
module.exports = UserRepository;
