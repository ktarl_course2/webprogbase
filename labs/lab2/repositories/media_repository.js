const MediaStorage = require('../storages/media_storage');

class MediaRepository {
    constructor(directory_path, id_file_path) {
        this.storage_ = new MediaStorage(directory_path, id_file_path);
    }

    get storage() {
        return this.storage_;
    }

    getMediaById(id) {
        const file = this.storage_.readMediaFile(id);
        return file;
    }

    addMedia(file_data, file_name) {
        const id = this.storage_.writeMediaFile(file_data, file_name);
        return id;
    }
}

module.exports = MediaRepository;