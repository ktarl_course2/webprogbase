module.exports = {
    getPaginationParams(page, per_page, default_page = 1, default_per_page = 10, default_max_per_page = 100) {
        if (typeof page != 'undefined') {
            try {
                page = Number.parseInt(page);
            }
            catch (e) {
                return null;
            }
            if (typeof per_page != 'undefined') { // page and per_page
                try {
                    per_page = Number.parseInt(per_page);
                }
                catch (e) {
                    return null;
                }
                return {
                    page: page,
                    per_page: per_page <= default_max_per_page ? per_page : default_max_per_page
                };
            }
            return {
                page: page,
                per_page: default_per_page
            };
        }
        return {
            page: default_page,
            per_page: default_per_page
        };
    },

    getFromToParams(page, per_page, max_number_of_entities) {
        return {
            from: (page - 1) * per_page,
            to: (page * per_page + 1 <= max_number_of_entities) ? (page * per_page + 1) : max_number_of_entities
        };
    }
}