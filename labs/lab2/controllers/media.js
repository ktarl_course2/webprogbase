const MediaRepository = require('../repositories/media_repository');
const validator = require('./validator');

const directory_path = "./data/media/";
const id_file_path = "./data/media/next_id.txt"
const media_repository = new MediaRepository(directory_path, id_file_path);

module.exports = {
    getMediaById(req, res) {
        const id_to_find = req.params.id;
        const media = media_repository.getMediaById(id_to_find);
        if (media === null) {
            res.status(404).send(JSON.stringify(`File with id ${id_to_find} not found`));
        }
        else {
            res.status(200).download(media.name);
        }
    },

    addMedia(req, res) {
        if (typeof req.body === 'undefined' || typeof req.files === 'undefined') {
            res.status(400).send('No file provided');
        }
        const file_data = req.files['image'].data;
        const file_name = req.files['image'].name;
        if (!validator.isFileNameFull(file_name)) {
            res.status(400).send(JSON.stringify(`Invalid file name: ${file_name}`));
        }
        else {
            const id = media_repository.addMedia(file_data, file_name);
            res.status(200).send(JSON.stringify(id));
        }
    }
};