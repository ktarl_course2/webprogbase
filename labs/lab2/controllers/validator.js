const Post = require('../models/post');
const User = require('../models/user');

module.exports = {
    checkAndGetPostIfValid(json_object) {
        const post = new Post();
        let required_keys = [];
        for (const key of Object.keys(post)) {
            if (key != "id" && key != "id_") {
                required_keys.push(key);
            }
        }
        for (const key of Object.keys(json_object)) {
            if (post.hasOwnProperty(key) || post.hasOwnProperty(key + "_")) {
                try {
                    post[key] = json_object[key];
                    required_keys = required_keys.filter(required_key => required_key != key && required_key != key+"_");
                }
                catch (e) {
                    console.log(e.message);
                    return { post: null, error: e.message};
                }
            }
            else {
                const error_message = `No such property in post: ${key}`
                return { post: null, error: error_message};
            }
        }
        if (required_keys.length > 0) {
            return { post: null, error: `Required data not provided: ${required_keys}`};
        }
        return {
            post: post,
            error: null
        };
    },

    checkAndGetIdIfValid(id_to_parse) {
        let parsed_id = 0;
        try {
            parsed_id = Number.parseInt(id_to_parse);
        }
        catch (error) {
            return {
                error: error,
                id: null
            };
        }
        return {
            error: null,
            id: parsed_id
        };
    },

    isFileNameFull(file_name) {
        const [name, format] = file_name.split('.');
        if (typeof name === 'undefined' || typeof format === 'undefined') {
            return false;
        }
        if (name.length === 0 || format.length === 0) {
            return false;
        }
        return true;
    }
};
