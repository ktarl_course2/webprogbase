const Post = require('../models/post');
const PostRepository = require('../repositories/post_repository');
const validator = require('./validator');
const page_handler = require('./page_handler');

const posts_file_path = "./data/posts.json";
const posts_id_file_path = "./data/posts_id.json";
const post_repository = new PostRepository(posts_file_path, posts_id_file_path);

module.exports = {
    getPosts(req, res) {
        let posts_array = post_repository.getPosts();
        const page = req.query.page;
        const per_page = req.query.page;
        const pagination_info = page_handler.getPaginationParams(page, per_page);
        if (pagination_info === null) {
            res.status(400).send(JSON.stringify(`Invalid parameters for pagination: ${page}, ${per_page}`))
        }
        else {
            const from_to_params = page_handler.getFromToParams(page, per_page, posts_array.length);
            // console.log(`${from_to_params.from}, ${from_to_params.to}`);
            posts_array = posts_array.slice(from_to_params.from, from_to_params.to);
            const json_array = JSON.stringify(posts_array);
            res.status(200).send(json_array);
        }
    },

    getPostById(req, res) {
        const id_info = validator.checkAndGetIdIfValid(req.params.id);
        if (id_info.id === null) {
            res.status(400).send(JSON.stringify(`Invalid id ${req.params.id}: ${id_info.error}`));
        }
        const id_to_find = id_info.id;
        const post = post_repository.getPostById(id_to_find);
        if (post === null) {
            res.status(404).send(JSON.stringify(`Post with id ${id_to_find} not found`));
        }
        else {
            const json_object = JSON.stringify(post);
            res.status(200).send(json_object);
        }
    },

    addPost(req, res) {
        const post_info = validator.checkAndGetPostIfValid(req.body);
        if (post_info.post === null) {
            res.status(400).send(JSON.stringify(post_info.error));
        }
        else {
            const id = post_repository.addPost(post_info.post);
            const post = post_repository.getPostById(id);
            res.status(201).send(JSON.stringify(post));
        }
    },

    updatePost(req, res) {
        const post_info = validator.checkAndGetPostIfValid(req.body);
        if (post_info.post === null) {
            res.status(400).send(JSON.stringify(post_info.error));
        }
        else {
            const id_of_post_to_update = post_info.post.id;
            const updated_post = post_repository.jsonObjectToPostModel(req.body);
            let posts_array = post_repository.getPosts();
            const index = posts_array.findIndex(post => post.id_ === id_of_post_to_update);
            if (index < 0) {
                const error_message = `Post with id ${id_of_post_to_update} does not exist`
                console.error(`${error_message}\n`);
                res.status(404).send(JSON.stringify(error_message));
            }
            else {
                posts_array[index] = updated_post; // update post at [index]
                post_repository.storage.writeItems(posts_array);
                const updated_post_in_repo = post_repository.getPostById(id_of_post_to_update);
                res.status(200).send(JSON.stringify(updated_post_in_repo));
            }
        }
    },

    deletePost(req, res) {
        const id_to_find = req.params.id;
        let posts_array = post_repository.getPosts();
        const index_post_to_delete = posts_array.findIndex(post => post.id_ == id_to_find);
        if (index_post_to_delete < 0) {
            const error_message = `Post with id ${id_to_find} does not exist`;
            console.error(`${error_message}\n`);
            res.status(404).send(JSON.stringify(error_message))
        }
        else {
            const post_to_delete = posts_array[index_post_to_delete];
            posts_array.splice(index_post_to_delete, 1);
            post_repository.storage.writeItems(posts_array);
            res.status(200).send(JSON.stringify(post_to_delete));
        }
    }
};
