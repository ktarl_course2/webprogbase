const User = require('../models/user');
const UserRepository = require('../repositories/user_repository');
const page_handler = require('./page_handler');
const validator = require('./validator');

const users_file_path = "./data/users.json";
const users_id_file_path = "./data/users_id.json";
const user_repository = new UserRepository(users_file_path, users_id_file_path);

module.exports = {
    getUsers(req, res) {
        let users_array = user_repository.getUsers();
        const page = req.query.page;
        const per_page = req.query.page;
        const pagination_info = page_handler.getPaginationParams(page, per_page);
        if (pagination_info === null) {
            res.status(400).send(JSON.stringify(`Invalid parameters for pagination: ${page}, ${per_page}`)).end();
        }
        const from_to_params = page_handler.getFromToParams(page, per_page, users_array.length);
        // console.log(`${from_to_params.from}, ${from_to_params.to}`);
        users_array = users_array.slice(from_to_params.from, from_to_params.to);
        const json_array = JSON.stringify(users_array);
        res.status(200).send(json_array).end();
    },

    getUserById(req, res) {
        const id = req.params.id;
        const id_info = validator.checkAndGetIdIfValid(id);
        if (id_info.id === null) {
            res.status(400).send(JSON.stringify(`Invalid id ${id}: ${id_info.error}`));
        }
        const id_to_find = id_info.id;
        const user = user_repository.getUserById(id_to_find);
        if (user === null) {
            res.status(404).send(JSON.stringify(`User with id ${id_to_find} not found`));
        }
        else {
            const json_object = JSON.stringify(user);
            res.status(200).send(json_object);
        }
    }
};
